package de.ithappens.q1;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Properties;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;

import de.ithappens.commons.usercontext.EnvironmentPreference;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * Copyright: 2019
 * Organization: IT-Happens.de
 * @author Ryppo
 */
public class Q1Preferences extends EnvironmentPreference {

    private static final String CONFIG_FILE_NAME = "kuh-eins.einstellungen";
    private final Properties configFile = new Properties();

    private final static String LAST_USED_TEMPLATE = "last-used-template";
    private final static String SURVEY_FOLDER = "survey-folder";
    private final static String EXPECTED_COUNT_OF_FILLED_QUESTIONAIRES = "expected-count-of-filled-questionaires";
    private final static String TOOLS_PASSWORD = "tools-password";
    private final static String SURVEY_TITLE = "survey-title";
    private final static String MAX_CODE_ROWS_PER_PAGE = "MAX_CODE_ROWS_PER_PAGE";

    private static boolean toolsEnabled = false;

    private final StringProperty surveyTitleProperty = new SimpleStringProperty();

    public Q1Preferences() throws FileNotFoundException, IOException {
	configFile.load(new FileReader(CONFIG_FILE_NAME));
	surveyTitleProperty.set(getSurveyTitle());
    }

    public int getMaxCodeRowsPerPage() {
	int defaultValue = 10;
	int configValue = getConfigInt(MAX_CODE_ROWS_PER_PAGE);
	return configValue > 0 ? configValue : defaultValue;
    }

    public void setMaxCodeRowsPerPage(int maxCodeRowsPerPage) {
	setConfigInt(MAX_CODE_ROWS_PER_PAGE, maxCodeRowsPerPage);
    }

    public StringProperty surveyTitleProperty() {
	return surveyTitleProperty;
    }

    public void setSurveyTitle(String surveyTitle) {
	setConfigString(SURVEY_TITLE, surveyTitle);
	surveyTitleProperty.set(surveyTitle);
    }

    public String getSurveyTitle() {
	return getConfigString(SURVEY_TITLE);
    }

    public String getLastUsedTemplate() {
	return getConfigString(LAST_USED_TEMPLATE);
    }

    public void setLastUsedTemplate(String value) {
	setConfigString(LAST_USED_TEMPLATE, value);
    }

    public String getSurveyFolder() {
	return getConfigString(SURVEY_FOLDER);
    }

    public void setSurveyFolder(String value) {
	setConfigString(SURVEY_FOLDER, value);
    }

    public void setToolsPassword(String value) {
	setConfigString(TOOLS_PASSWORD, value);
    }

    public String getToolsPassword() {
	return getConfigString(TOOLS_PASSWORD);
    }

    public void authorizeForTools(String toolsPassword) {
	toolsEnabled = StringUtils.equals(getToolsPassword(), toolsPassword);
    }

    public boolean isToolsEnabled() {
	return toolsEnabled;
    }

    public void setToolsEnabled(boolean enabled) {
	toolsEnabled = enabled;
    }

    public int getExpectedCountOfFilledQuestionaires() {
	return getConfigInt(EXPECTED_COUNT_OF_FILLED_QUESTIONAIRES);
    }

    public void setExpectedCountOfFilledQuestionaires(int value) {
	setConfigInt(EXPECTED_COUNT_OF_FILLED_QUESTIONAIRES, value);
    }

    private void setConfigString(String key, String value) {
	writeToConfigFile(key, value);
    }

    private void setConfigInt(String key, int value) {
	writeToConfigFile(key, Integer.toString(value));
    }

    private String getConfigString(String key) {
	return configFile.getProperty(key);
    }

    private FileWriter writer = null;

    private void writeToConfigFile(String key, String value) {
	try {
	    configFile.setProperty(key, value);
	    if (writer == null) {
		writer = new FileWriter(CONFIG_FILE_NAME);
	    }
	    configFile.store(writer, "schu-bi-du");
	} catch (IOException e) {
	    e.printStackTrace();
	}
    }

    private int getConfigInt(String key) {
	String stringValue = configFile.getProperty(key);
	if (NumberUtils.isCreatable(stringValue)) {
	    return Integer.parseInt(stringValue);
	} else {
	    return 0;
	}
    }

    @Override
    public CONTEXT getContext() {
	return CONTEXT.USER;
    }

}
