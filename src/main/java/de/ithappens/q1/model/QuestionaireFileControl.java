package de.ithappens.q1.model;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Stream;

import org.apache.commons.lang3.StringUtils;

import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;

@SuppressWarnings("deprecation")
public class QuestionaireFileControl {

	private static final char CSV_SEPARATOR = ";".charAt(0);
	private static final String QUESTIONAIRE_POSTFIX = ".q1q";

	public static Questionaire loadQuestions(File sourceFile, boolean withRating) throws IOException {
		Questionaire questionaire = new Questionaire();
		FileReader filereader = new FileReader(sourceFile);
		CSVReader csvReader = new CSVReader(filereader, CSV_SEPARATOR);
		String[] nextRecord;
		while ((nextRecord = csvReader.readNext()) != null) {
			if (withRating) {
				questionaire.questionsProperty().add(new Question(nextRecord[0], nextRecord[1], nextRecord[2]));
			} else {
				questionaire.questionsProperty().add(new Question(nextRecord[0], nextRecord[1], nextRecord[2],
						nextRecord[3], nextRecord[4], nextRecord[5]));
			}
		}
		csvReader.close();
		return questionaire;
	}

	public static Questionaire loadQuestions(String sourceFileName, boolean widthRatings) throws IOException {
		return loadQuestions(new File(sourceFileName), widthRatings);
	}

	public static void saveQuestionaire(String surveyFolder, Questionaire questionaire, String userCode)
			throws IOException {
		FileWriter fileWriter = new FileWriter(surveyFolder + "/" + StringUtils.join(userCode, QUESTIONAIRE_POSTFIX),
				false);
		CSVWriter csvWriter = new CSVWriter(fileWriter, CSV_SEPARATOR);
		questionaire.questionsProperty().stream().forEach((q) -> {

			csvWriter.writeNext(new String[] { Integer.toString(q.idProperty().get()), q.textProperty().get(),
					Double.toString(q.ratingProperty().get()) });
		});
		csvWriter.close();
	}

	public static SurveyResult loadSurveyResult(String dirName) throws IOException {
		SurveyResult surveyResult = new SurveyResult();
		try (Stream<Path> paths = Files.walk(Paths.get(dirName), 2)) {
			paths.map(path -> path.toString()).filter(f -> f.endsWith(QUESTIONAIRE_POSTFIX)).forEach(t -> {
				try {
					surveyResult.add(QuestionaireFileControl.loadQuestions(t, true));
				} catch (IOException e) {
					e.printStackTrace();
				}
			});
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return surveyResult;
	}

}
