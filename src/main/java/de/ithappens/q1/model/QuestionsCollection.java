package de.ithappens.q1.model;

import java.util.ArrayList;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public abstract class QuestionsCollection<E extends Question> {

    private ObservableList<E> questionsProperty = FXCollections.observableArrayList(new ArrayList<E>());

    public ObservableList<E> questionsProperty() {
	return questionsProperty;
    }

    public boolean isAllRated() {
	return questionsProperty.stream().allMatch(E::isRated);
    }

}
