package de.ithappens.q1.model;

import java.util.ArrayList;

public class SurveyResult extends ArrayList<Questionaire> {

	public int countQuestions() {
		int countQuestions = 0;
		if (size() > 0) {
			for (Questionaire questionaire : this) {
				if (questionaire.questionsProperty().size() > countQuestions) {
					countQuestions = questionaire.questionsProperty().size();
				}
			}
		}
		return countQuestions;
	}

	public QuestionaireReport reportQuestionsResult() {
		QuestionaireReport report = new QuestionaireReport();
		int questionsCount = countQuestions();
		if (questionsCount > 0) {
			for (int pos = 0; pos < questionsCount; pos++) {
				QuestionReport questionReport = new QuestionReport(getQuestion(pos));
				questionReport.ratingProperty().set(calculateAverageRating(pos));
				questionReport.highestRatingProperty().set(calculateMaximumRating(pos));
				questionReport.lowestRatingProperty().set(calculateMinimumRating(pos));
				questionReport.countRatingsProperty().set(calculatecountRatings(pos));
				questionReport.count1Property().set(calculateCount1(pos));
				questionReport.count2Property().set(calculateCount2(pos));
				questionReport.count3Property().set(calculateCount3(pos));
				questionReport.count4Property().set(calculateCount4(pos));
				questionReport.count5Property().set(calculateCount5(pos));
				report.questionsProperty().add(questionReport);
			}
		}
		return report;

	}

	public double calculateAverageRating(int questionNumber) {
		double summary = calculateSummaryRating(questionNumber);
		return summary == 0.0 ? 0.0 : summary / size();
	}

	public Question getQuestion(int questionNumber) {
		if (size() > 0) {
			for (Questionaire questionaire : this) {
				if (questionaire.questionsProperty().size() > questionNumber) {
					return questionaire.questionsProperty().get(questionNumber);
				}
			}
		}
		return null;
	}

	public double calculateSummaryRating(int questionNumber) {
		double summary = 0.0;
		for (Questionaire questionaire : this) {
			if (questionaire.questionsProperty().size() > questionNumber) {
				summary += questionaire.questionsProperty().get(questionNumber).ratingProperty().get();
			}
		}
		return summary;
	}

	public int calculatecountRatings(int questionNumber) {
		int countRatings = 0;
		for (Questionaire questionaire : this) {
			if (questionaire.questionsProperty().size() > questionNumber
					&& questionaire.questionsProperty().get(questionNumber).ratingProperty().get() > 0.0) {
				countRatings++;
			}
		}
		return countRatings;
	}

	public int calculateCount1(int questionNumber) {
		return calculateCount(questionNumber, 1.0);
	}

	public int calculateCount2(int questionNumber) {
		return calculateCount(questionNumber, 2.0);
	}

	public int calculateCount3(int questionNumber) {
		return calculateCount(questionNumber, 3.0);
	}

	public int calculateCount4(int questionNumber) {
		return calculateCount(questionNumber, 4.0);
	}

	public int calculateCount5(int questionNumber) {
		return calculateCount(questionNumber, 5.0);
	}

	private int calculateCount(int questionNumber, double expectedRating) {
		int count = 0;
		for (Questionaire questionaire : this) {
			if (questionaire.questionsProperty().size() > questionNumber
					&& questionaire.questionsProperty().get(questionNumber).ratingProperty().get() == expectedRating) {
				count++;
			}
		}
		return count;
	}

	public double calculateMinimumRating(int questionNumber) {
		double minimum = 5.0;
		for (Questionaire questionaire : this) {
			if (questionaire.questionsProperty().size() > questionNumber) {
				double currentRating = questionaire.questionsProperty().get(questionNumber).ratingProperty().get();
				if (minimum > currentRating) {
					minimum = currentRating;
				}
			}
		}
		return minimum;
	}

	public double calculateMaximumRating(int questionNumber) {
		double maximum = 0.0;
		for (Questionaire questionaire : this) {
			if (questionaire.questionsProperty().size() > questionNumber) {
				double currentRating = questionaire.questionsProperty().get(questionNumber).ratingProperty().get();
				if (maximum < currentRating) {
					maximum = currentRating;
				}
			}
		}
		return maximum;
	}

}
