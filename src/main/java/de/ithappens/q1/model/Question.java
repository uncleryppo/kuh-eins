package de.ithappens.q1.model;

import org.apache.commons.lang3.math.NumberUtils;

import javafx.beans.binding.Bindings;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Question {

    private final IntegerProperty idProperty = new SimpleIntegerProperty();
    private final StringProperty textProperty = new SimpleStringProperty();
    private final DoubleProperty ratingProperty = new SimpleDoubleProperty();
    private final BooleanProperty ratedProperty = new SimpleBooleanProperty();
    private final StringProperty answer1Property = new SimpleStringProperty();
    private final StringProperty answer2Property = new SimpleStringProperty();
    private final StringProperty answer3Property = new SimpleStringProperty();
    private final StringProperty answer4Property = new SimpleStringProperty();

    public IntegerProperty idProperty() {
	return idProperty;
    }

    public void setId(int id) {
	idProperty.set(id);
    }

    public StringProperty textProperty() {
	return textProperty;
    }

    public void setText(String text) {
	textProperty.set(text);
    }

    public Question(int _id, String _text) {
	setId(_id);
	setText(_text);
    }

    public Question(String _id, String _text, String _answer1, String _answer2, String _answer3, String _answer4) {
	init(_id, _text, null);
	answer1Property.set(_answer1);
	answer2Property.set(_answer2);
	answer3Property.set(_answer3);
	answer4Property.set(_answer4);
    }

    public Question(String _id, String _text, String _rating) {
	init(_id, _text, _rating);
    }

    private void init(String _id, String _text, String _rating) {
	if (NumberUtils.isCreatable(_id)) {
	    setId(NumberUtils.toInt(_id));
	}
	setText(_text);
	if (NumberUtils.isCreatable(_rating)) {
	    ratingProperty.set(NumberUtils.toDouble(_rating));
	}
	ratedProperty.bind(Bindings.createBooleanBinding(() -> {
	    return ratingProperty.get() > 0.0d;
	}, ratingProperty));
    }

    public DoubleProperty ratingProperty() {
	return ratingProperty;
    }

    public boolean isRated() {
	return ratingProperty.get() > 0.0d;
    }

    public BooleanProperty ratedProperty() {
	return ratedProperty;
    }

    public String[] getAnswers() {
	return new String[] { answer1Property.get(), answer2Property.get(), answer3Property.get(),
		answer4Property.get() };
    }

}
