package de.ithappens.q1.model;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;

public class QuestionReport extends Question {

    private final DoubleProperty highestRatingProperty = new SimpleDoubleProperty();
    private final DoubleProperty lowestRatingProperty = new SimpleDoubleProperty();
    private final IntegerProperty countRatingsProperty = new SimpleIntegerProperty();
    private final IntegerProperty count0Property = new SimpleIntegerProperty();
    private final IntegerProperty count1Property = new SimpleIntegerProperty();
    private final IntegerProperty count2Property = new SimpleIntegerProperty();
    private final IntegerProperty count3Property = new SimpleIntegerProperty();
    private final IntegerProperty count4Property = new SimpleIntegerProperty();
    private final IntegerProperty count5Property = new SimpleIntegerProperty();

    public QuestionReport(String _id, String _text, String _rating) {
	super(_id, _text, _rating);
    }

    public QuestionReport(Question source) {
	super(Integer.toString(source.idProperty().get()), source.textProperty().get(),
		Double.toString(source.ratingProperty().get()));
    }

    public DoubleProperty highestRatingProperty() {
	return highestRatingProperty;
    }

    public DoubleProperty lowestRatingProperty() {
	return lowestRatingProperty;
    }

    public IntegerProperty count0Property() {
	return count0Property;
    }

    public IntegerProperty count1Property() {
	return count1Property;
    }

    public IntegerProperty count2Property() {
	return count2Property;
    }

    public IntegerProperty count3Property() {
	return count3Property;
    }

    public IntegerProperty count4Property() {
	return count4Property;
    }

    public IntegerProperty count5Property() {
	return count5Property;
    }

    public IntegerProperty countRatingsProperty() {
	return countRatingsProperty;
    }

}
