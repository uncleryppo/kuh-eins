package de.ithappens.q1;

import org.apache.commons.lang3.StringUtils;

import com.dlsc.workbenchfx.model.WorkbenchModule;
import com.dlsc.workbenchfx.view.controls.ToolbarItem;

import de.ithappens.commons.usercontext.UserContext;
import de.ithappens.q1.core.Y3AppController;
import de.ithappens.q1.core.Y3WorkbenchModule;
import de.ithappens.q1.fx.FxTools;
import de.ithappens.q1.module.Q1PreferencesModule;
import de.ithappens.q1.module.QCodesModule;
import de.ithappens.q1.module.QForm1Module;
import de.ithappens.q1.module.QStats1Module;
import de.jensd.fx.glyphs.materialdesignicons.MaterialDesignIcon;
import de.jensd.fx.glyphs.materialdesignicons.MaterialDesignIconView;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.PasswordField;
import javafx.scene.image.Image;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.HBox;

/**
 * Copyright: 2019
 * Organization: IT-Happens.de
 * @author Ryppo
 */
public class Q1Controller extends Y3AppController {

    private final Q1Preferences PREFS = UserContext.getInstance().getEnvironment(Q1Preferences.class);
    private Button switchOffTools_btn;
    private MenuItem switchOnTools_mi;
    private PasswordField toolsPassword_pwf;
    private ToolbarItem switchOffTools_tbi, switchOnTools_tbi;

    private Y3WorkbenchModule q1StatsModule, qCodesModule, prefsModule;

    @Override
    public void registerModules() {
	register(SCOPE.Q_A, new QForm1Module(this));
    }

    @Override
    public SCOPE getDefaultScope() {
	return SCOPE.Q_A;
    }

    @Override
    public String getAboutFxml() {
	return "about.fxml";
    }

    @Override
    public Image getImage() {
	return FxTools.APP_ICON;
    }

    @Override
    public String getApplicationTitle() {
	return FxTools.APP_TITLE;
    }

    @Override
    public void initUi() {
	q1StatsModule = new QStats1Module(this);
	qCodesModule = new QCodesModule(this);
	prefsModule = new Q1PreferencesModule(this);

	switchOffTools_btn = new Button(FxTools.SWITCH_OFF_TOOLS);
	switchOffTools_btn.setOnMouseReleased((e) -> {
	    actionSwitchOffTools();
	});
	switchOffTools_btn.setStyle("-fx-text-fill: green");
	switchOffTools_tbi = new ToolbarItem(switchOffTools_btn);

	Button switchOnTools_btn = new Button("", new MaterialDesignIconView(MaterialDesignIcon.LOCK_OPEN));
	switchOnTools_btn.setOnMouseReleased((e) -> {
	    actionSwitchOnTools();
	});
	toolsPassword_pwf = new PasswordField();
	toolsPassword_pwf.setOnKeyPressed((e) -> {
	    if (KeyCode.ENTER.equals(e.getCode())) {
		actionSwitchOnTools();
	    }
	});
	Label password_lbl = new Label(FxTools.PASSWORD);
	password_lbl.setMaxHeight(Double.MAX_VALUE);
	password_lbl.setMaxWidth(Double.MAX_VALUE);
	switchOnTools_mi = new MenuItem("", new HBox(5d, password_lbl, toolsPassword_pwf, switchOnTools_btn));
	switchOnTools_tbi = new ToolbarItem(FxTools.SWITCH_ON_TOOLS,
		new MaterialDesignIconView(MaterialDesignIcon.ACCOUNT), switchOnTools_mi);
	getWorkbench().getToolbarControlsRight().add(switchOnTools_tbi);
    }

    private void actionSwitchOffTools() {
	unregister(SCOPE.REPORT, q1StatsModule);
	unregister(SCOPE.CODES, qCodesModule);
	unregister(SCOPE.PREFS, prefsModule);
	PREFS.setToolsEnabled(false);
	getWorkbench().getToolbarControlsRight().remove(switchOffTools_tbi);
	getWorkbench().getToolbarControlsRight().add(switchOnTools_tbi);
	toolsPassword_pwf.setText(null);
    }

    private void actionSwitchOnTools() {
	if (StringUtils.equals(PREFS.getToolsPassword(), toolsPassword_pwf.getText())) {
	    WorkbenchModule activeModule = getWorkbench().getActiveModule();
	    register(SCOPE.REPORT, q1StatsModule);
	    register(SCOPE.CODES, qCodesModule);
	    register(SCOPE.PREFS, prefsModule);
	    PREFS.setToolsEnabled(true);
	    getWorkbench().getToolbarControlsRight().remove(switchOnTools_tbi);
	    getWorkbench().getToolbarControlsRight().add(switchOffTools_tbi);
	    if (activeModule != null) {
		getWorkbench().openModule(activeModule);
	    }
	} else {
	    getWorkbench().showErrorDialog(FxTools.PASSWORD, FxTools.NO_THIS_IS_NOT_CORREKT, buttonType1 -> {
	    });
	}
    }

    @Override
    public boolean isAllowedToOpen(SCOPE _scope) {
	switch (_scope) {
	case PREFS:
	case REPORT:
	    return PREFS.isToolsEnabled();
	default:
	    return true;
	}
    }

}
