package de.ithappens.q1.fx;

import java.util.ArrayList;
import java.util.List;

import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

public class RatingPane extends HBox {

    private IntegerProperty ratingProperty = new SimpleIntegerProperty();

    private int maxValue = 0;
    private String[] ratingTexts;
    boolean ratingValueAscending = true;

    private List<RatingArea> ratingAreas = new ArrayList<>();
    private List<Button> ratingsButtons = new ArrayList<>();

    public RatingPane(int _max, String[] _ratingTexts, boolean _ratingValueAscending) {
	maxValue = _max;
	ratingTexts = _ratingTexts;
	buildUi();
    }

    public IntegerProperty ratingProperty() {
	return ratingProperty;
    }

    public int getRating() {
	return ratingProperty.get();
    }

    public void setRating(int _value) {
	ratingProperty.set(_value);
	clearRatingButtons();
    }

    private final String FONT_10_PX = "-fx-font-size: 10px; ";
    private final String FONT_12_PX = "-fx-font-size: 12px; ";
    private final String FONT_14_PX = "-fx-font-size: 14px; ";
    private final String BACKGROUND_COLOR_BLUE = "-fx-background-color: blue";
    private final String BACKGROUND_COLOR_LIGHTBLUE = "-fx-background-color: lightblue";
    private final String BACKGROUND_COLOR_GREY = "-fx-background-color: lightgrey";

    private void buildUi() {
	getChildren().removeAll(ratingAreas);
	ratingAreas.clear();
	ratingsButtons.clear();
	for (int pos = 0; pos < maxValue; pos++) {
	    RatingArea ratingArea = new RatingArea(getRatingText(pos), getRatingValue(pos));
	    ratingAreas.add(ratingArea);
	}
	getChildren().addAll(ratingAreas);
    }

    private void clearRatingButtons() {
	for (RatingArea ratingArea : ratingAreas) {
	    ratingArea.formatForNormal();
	}
    }

    public String getRatingText(int pos) {
	return ratingTexts != null && ratingTexts.length > pos ? ratingTexts[pos]
		: Integer.toString(getRatingValue(pos));
    }

    private int getRatingValue(int pos) {
	return ratingValueAscending ? (pos + 1) : (maxValue - pos + 1);
    }

    class RatingArea extends VBox {

	Button button;
	Label label;
	int value = 0;

	public RatingArea(String labelText, int _value) {
	    button = new Button("", new FontAwesomeIconView(FontAwesomeIcon.STAR));
	    label = new Label(labelText);
	    value = _value;
	    getChildren().add(button);
	    getChildren().add(label);
	    setPrefWidth(80d);
	    setAlignment(Pos.CENTER);
	    button.setOnMouseEntered((e) -> {
		formatForHover();
	    });
	    button.setOnMouseExited((e) -> {
		if (getRating() == value) {
		    formatForSelected();
		} else {
		    formatForNormal();
		}
	    });
	    button.setOnMouseReleased(((e) -> {
		setRating(value);
		formatForSelected();
	    }));
	    formatForNormal();
	}

	public void formatForNormal() {
	    button.setStyle(FONT_10_PX + BACKGROUND_COLOR_GREY);
	    label.setStyle(FONT_14_PX);
	}

	public void formatForHover() {
	    button.setStyle(FONT_10_PX + BACKGROUND_COLOR_LIGHTBLUE);
	    label.setStyle(FONT_14_PX);
	}

	public void formatForSelected() {
	    clearRatingButtons();
	    button.setStyle(FONT_10_PX + BACKGROUND_COLOR_BLUE);
	    label.setStyle(FONT_14_PX);
	}

    }

}
