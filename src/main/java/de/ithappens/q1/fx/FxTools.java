package de.ithappens.q1.fx;

import java.io.IOException;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.text.StringEscapeUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.image.Image;

/**
 * Copyright: 2019
 * Organization: IT-Happens.de
 * @author Ryppo
 */
public class FxTools {

    private final static Logger log = LogManager.getLogger(FxTools.class);
    // private final static SimpleDateFormat DF = new SimpleDateFormat("dd-MMM-yyyy
    // HH:mm:ss.SSS");

    private final static String BUNDLE_NAME = "de.ithappens.q1";
    private final static ResourceBundle RB = ResourceBundle.getBundle(BUNDLE_NAME);

    public final static String APP_TITLE = get("APP_TITLE");
    public final static String Q_FORM = get("Q_FORM");
    public final static String Q_PREFERENCES = get("Q_PREFERENCES");
    public final static String Q_STATS = get("Q_STATS");
    public final static String DID_YOU_FINISH = get("DID_YOU_FINISH");
    public final static String NOT_YET = get("NOT_YET");
    public final static String FIRST_ANSWER_ALL_QUESTIONS = get("FIRST_ANSWER_ALL_QUESTIONS");
    public final static String FIRST_ENTER_YOUR_CODE = get("FIRST_ENTER_YOUR_CODE");
    public final static String THANK_YOU = get("THANK_YOU");
    public final static String THE_QUESTIONAIRE_IS_SAVED = get("THE_QUESTIONAIRE_IS_SAVED");
    public final static String COUNT_FILLED_QUESTIONAIRES = get("COUNT_FILLED_QUESTIONAIRES");
    public final static String AVERAGE_RATINGS = get("AVERAGE_RATINGS");
    public final static String SWITCH_ON_TOOLS = get("SWITCH_ON_TOOLS");
    public final static String SWITCH_OFF_TOOLS = get("SWITCH_OFF_TOOLS");
    public final static String PASSWORD = get("PASSWORD");
    public final static String NO_THIS_IS_NOT_CORREKT = get("NO_THIS_IS_NOT_CORREKT");
    public final static String TEMPLATE = get("TEMPLATE");
    public final static String PRINT_CODES = get("PRINT_CODES");
    public final static String PREVIEW_AND_PRINT = get("PREVIEW_AND_PRINT");
    public final static String QUESTIONAIRE_RESULT = get("QUESTIONAIRE_RESULT");

    public final static String SAVE = get("SAVE");

    @SuppressWarnings("finally")
    private static String get(String key) {
	String value = null;
	try {
	    value = RB.getString(key);
	} catch (Exception ex) {
	    ex.printStackTrace();
	    value = "<TEXT_ERROR>";
	} finally {
	    if (value == null || value.equals("")) {
		value = key;
	    }
	    return StringEscapeUtils.unescapeJava(value);
	}
    }

    private final static String FX_LOCATION = "de/ithappens/q1/fx/";

    public static FXMLLoader getFXMLLoader(String fxmlFilename) {
	FXMLLoader loader = null;
	try {
	    ClassLoader classLoader = FxTools.class.getClassLoader();
	    String bundleFilename = StringUtils.replace(BUNDLE_NAME, ".", "/") + ".properties";
	    log.debug("bundleFilename: " + bundleFilename);
	    loader = new FXMLLoader(classLoader.getResource(FX_LOCATION + fxmlFilename),
		    new PropertyResourceBundle(classLoader.getResource(bundleFilename).openStream()));
	} catch (IOException iox) {
	    iox.printStackTrace();
	    log.error(iox);
	}
	return loader;
    }

    public static Parent loadFxml(FXMLLoader loader) {
	Parent parent = null;
	try {
	    parent = (Parent) loader.load();
	} catch (IOException iox) {
	    iox.printStackTrace();
	    log.error(iox);
	}
	return parent;
    }

    public final static Image APP_ICON = getAppIcon("q1_symbol_64.png");
    public final static Image Q1_LOGO = getAppIcon("q1_logo.jpg");

    @SuppressWarnings("finally")
    private static Image getAppIcon(String imageFilename) {
	Image theImage = null;
	ClassLoader classLoader = FxTools.class.getClassLoader();
	try {
	    theImage = new Image(classLoader.getResource(FX_LOCATION + imageFilename).openStream());
	} catch (IOException e) {
	    e.printStackTrace();
	} finally {
	    return theImage;
	}
    }

}
