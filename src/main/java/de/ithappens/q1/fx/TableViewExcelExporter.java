package de.ithappens.q1.fx;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import de.ithappens.q1.model.QuestionReport;
import javafx.collections.ObservableList;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;

/**
 * Copyright: 2019
 * Organization: IT-Happens.de
 * @author Ryppo
 */
public class TableViewExcelExporter {

    public static File export(TableView<QuestionReport> tableView, String exportFilename, String sheetName)
	    throws EncryptedDocumentException, IOException {
	File exportedFile = null;
	if (tableView != null && exportFilename != null) {
	    Workbook workbook = new XSSFWorkbook();
	    Sheet spreadsheet = workbook.createSheet(sheetName);
	    int mainHeaderRowId = 0;
	    Row xRow = spreadsheet.createRow(mainHeaderRowId);
	    ObservableList<TableColumn<QuestionReport, ?>> columns = tableView.getColumns();
	    CellStyle headerStyle = workbook.createCellStyle();
	    Font headerFont = workbook.createFont();
	    headerFont.setBold(true);
	    headerFont.setColor(IndexedColors.WHITE.getIndex());
	    headerStyle.setFont(headerFont);
	    headerStyle.setFillForegroundColor(IndexedColors.BLUE.getIndex());
	    headerStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
	    int firstDataRowNo = 1;
	    int xCurrentColId = 0;
	    boolean subColsInUse = false;
	    for (int colNo = 0; colNo < columns.size(); colNo++) {
		TableColumn<QuestionReport, ?> tableColumn = columns.get(colNo);
		String columnName = tableColumn.getText();
		Cell createdCell = xRow.createCell(xCurrentColId);
		createdCell.setCellValue(columnName);
		createdCell.setCellStyle(headerStyle);
		ObservableList<TableColumn<QuestionReport, ?>> subColumns = tableColumn.getColumns();
		// handle sub header column
		int subColCount = subColumns.size();
		if (subColCount > 0) {
		    subColsInUse = true;
		    // adjust main column
		    int lastSubColId = xCurrentColId + subColCount - 1;
		    spreadsheet.addMergedRegion(
			    new CellRangeAddress(mainHeaderRowId, mainHeaderRowId, xCurrentColId, lastSubColId));
		    // add sub header
		    Row xSubRow = spreadsheet.getRow(1) != null ? spreadsheet.getRow(1) : spreadsheet.createRow(1);
		    for (int subColNo = 0; subColNo < subColCount; subColNo++) {
			TableColumn<QuestionReport, ?> subColumn = subColumns.get(subColNo);
			String subColName = subColumn.getText();
			Cell subCell = xSubRow.createCell(xCurrentColId);
			subCell.setCellValue(subColName);
			subCell.setCellStyle(headerStyle);
			xCurrentColId++;
		    }
		    xCurrentColId--;
		} else {
		}
		xCurrentColId++;
	    }
	    if (subColsInUse) {
		spreadsheet.createFreezePane(0, 2);
		firstDataRowNo++;
	    } else {
		spreadsheet.createFreezePane(0, 1);
	    }
	    for (int rowNo = 0; rowNo < tableView.getItems().size(); rowNo++) {
		xRow = spreadsheet.createRow(rowNo + firstDataRowNo);
		xCurrentColId = 0;
		for (int colNo = 0; colNo < columns.size(); colNo++) {
		    TableColumn<QuestionReport, ?> tableColumn = columns.get(colNo);
		    ObservableList<TableColumn<QuestionReport, ?>> subColumns = tableColumn.getColumns();
		    int subColCount = subColumns.size();
		    if (subColCount > 0) {
			for (int subColNo = 0; subColNo < subColCount; subColNo++) {
			    TableColumn<QuestionReport, ?> subColumn = subColumns.get(subColNo);
			    Object cellData = subColumn.getCellData(rowNo);
			    String xCellValue = "";
			    if (cellData != null) {
				xCellValue = cellData.toString();
			    }
			    xRow.createCell(xCurrentColId).setCellValue(xCellValue);
			    xCurrentColId++;
			}
			xCurrentColId--;
		    } else {
			Object cellData = tableColumn.getCellData(rowNo);
			String xCellValue = "";
			if (cellData != null) {
			    xCellValue = cellData.toString();
			}
			xRow.createCell(xCurrentColId).setCellValue(xCellValue);
		    }
		    spreadsheet.autoSizeColumn(colNo, true);
		    xCurrentColId++;
		}
	    }
	    exportedFile = File.createTempFile(exportFilename, ".xlsx");
	    FileOutputStream fileOutputStream = new FileOutputStream(exportedFile);
	    workbook.write(fileOutputStream);
	    fileOutputStream.close();
	    workbook.close();
	}
	return exportedFile;
    }

}
