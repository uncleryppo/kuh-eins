package de.ithappens.q1.module;

import de.ithappens.q1.core.Y3AppController;
import de.ithappens.q1.core.Y3WorkbenchModule;
import de.ithappens.q1.fx.FxTools;
import de.jensd.fx.glyphs.materialdesignicons.MaterialDesignIcon;

/**
 * Copyright: 2019
 * 
 * @author Christian.Rybotycky
 */
public class QForm1Module extends Y3WorkbenchModule {

	public QForm1Module(Y3AppController mainController) {
		super(mainController, FxTools.Q_FORM, MaterialDesignIcon.COMMENT_QUESTION_OUTLINE, "q-form-1.fxml");
	}

}
