package de.ithappens.q1.module;

import de.ithappens.q1.core.Y3AppController;
import de.ithappens.q1.core.Y3WorkbenchModule;
import de.ithappens.q1.fx.FxTools;
import de.jensd.fx.glyphs.materialdesignicons.MaterialDesignIcon;

public class QCodesModule extends Y3WorkbenchModule {
	
	public QCodesModule(Y3AppController mainController) {
		super(mainController, FxTools.PRINT_CODES, MaterialDesignIcon.ANIMATION, "print-codes.fxml");
	}

}
