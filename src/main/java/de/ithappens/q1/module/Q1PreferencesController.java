package de.ithappens.q1.module;

import java.io.File;

import org.apache.commons.lang3.math.NumberUtils;

import de.ithappens.commons.usercontext.UserContext;
import de.ithappens.q1.Q1Preferences;
import de.ithappens.q1.core.Y3WorkbenchController;
import de.ithappens.q1.fx.FxTools;
import javafx.fxml.FXML;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;

/**
 * Copyright: 2019
 * Organization: IT-Happens.de
 * @author Ryppo
 */
public class Q1PreferencesController extends Y3WorkbenchController {

    private Q1Preferences PREFS = UserContext.getInstance().getEnvironment(Q1Preferences.class);

    @FXML
    private TextField expectedCountOfFilledQuestionaires_txf, surveyTitle_txf, maxCountCodeRowsPerPage_txf;

    @FXML
    private PasswordField toolsPassword_pwf;

    @FXML
    public void actionChangeExpectedCountOfFilledQuestionaires() {
	if (NumberUtils.isParsable(expectedCountOfFilledQuestionaires_txf.getText())) {
	    PREFS.setExpectedCountOfFilledQuestionaires(
		    Integer.parseInt(expectedCountOfFilledQuestionaires_txf.getText()));
	}
    }

    @FXML
    public void actionMaxCountCodeRowsPerPageChanged() {
	if (NumberUtils.isParsable(maxCountCodeRowsPerPage_txf.getText())) {
	    PREFS.setMaxCodeRowsPerPage(Integer.parseInt(maxCountCodeRowsPerPage_txf.getText()));
	}
    }

    @FXML
    public void actionSurveyTitleChanged() {
	PREFS.setSurveyTitle(surveyTitle_txf.getText());
    }

    @FXML
    public void actionToolsPasswordChanged() {
	PREFS.setToolsPassword(toolsPassword_pwf.getText());
    }

    private static final String QUESTIONAIRE_TEMPLATE_POSTFIX = ".q1t";

    @FXML
    public void actionSelectTemplate() {
	FileChooser templateFileChooser = new FileChooser();
	templateFileChooser.setInitialFileName(PREFS.getLastUsedTemplate());
	templateFileChooser
		.setSelectedExtensionFilter(new ExtensionFilter(FxTools.TEMPLATE, QUESTIONAIRE_TEMPLATE_POSTFIX));
	;
	File selectedTemplate = templateFileChooser.showOpenDialog(gui().getWorkbench().getScene().getWindow());
	if (selectedTemplate != null) {
	    PREFS.setLastUsedTemplate(selectedTemplate.getAbsolutePath());
	    template_txf.setText(PREFS.getLastUsedTemplate());
	}
    }

    @FXML
    public void actionSelectSurveyFolder() {
	DirectoryChooser questionaireFolderChooser = new DirectoryChooser();
	File lastFolder = new File(PREFS.getSurveyFolder());
	if (lastFolder.exists()) {
	    questionaireFolderChooser.setInitialDirectory(lastFolder);
	}
	File selectedFolder = questionaireFolderChooser.showDialog(gui().getWorkbench().getScene().getWindow());
	if (selectedFolder != null) {
	    PREFS.setSurveyFolder(selectedFolder.getAbsolutePath());
	    survey_txf.setText(PREFS.getSurveyFolder());
	}
    }

    @FXML
    private TextField template_txf;

    @FXML
    private TextField survey_txf;

    @Override
    public void buildUi() {
	template_txf.setText(PREFS.getLastUsedTemplate());
	survey_txf.setText(PREFS.getSurveyFolder());
	expectedCountOfFilledQuestionaires_txf.setText(Integer.toString(PREFS.getExpectedCountOfFilledQuestionaires()));
	maxCountCodeRowsPerPage_txf.setText(Integer.toString(PREFS.getMaxCodeRowsPerPage()));
	toolsPassword_pwf.setText(PREFS.getToolsPassword());
	surveyTitle_txf.setText(PREFS.getSurveyTitle());
    }

    @Override
    public void openModel(Object model) {
	// TODO Auto-generated method stub

    }

    @Override
    public boolean isCompatibleModel(Object model) {
	// TODO Auto-generated method stub
	return false;
    }

    @Override
    public void reset() {
	template_txf.setText(null);
	survey_txf.setText(null);
	expectedCountOfFilledQuestionaires_txf.setText(null);
	maxCountCodeRowsPerPage_txf.setText(null);
	toolsPassword_pwf.setText(null);
	surveyTitle_txf.setText(null);
    }

    @Override
    public void refresh() {
	buildUi();
    }

}
