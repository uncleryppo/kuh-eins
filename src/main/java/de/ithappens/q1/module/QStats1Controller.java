package de.ithappens.q1.module;

import java.io.File;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import de.ithappens.commons.usercontext.UserContext;
import de.ithappens.q1.Q1Preferences;
import de.ithappens.q1.core.Y3WorkbenchController;
import de.ithappens.q1.fx.FxTools;
import de.ithappens.q1.fx.TableViewExcelExporter;
import de.ithappens.q1.model.QuestionReport;
import de.ithappens.q1.model.QuestionaireFileControl;
import de.ithappens.q1.model.SurveyResult;
import eu.hansolo.tilesfx.Tile;
import eu.hansolo.tilesfx.Tile.ImageMask;
import eu.hansolo.tilesfx.Tile.SkinType;
import eu.hansolo.tilesfx.chart.ChartData;
import javafx.beans.binding.Bindings;
import javafx.beans.property.SimpleStringProperty;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.layout.Region;

/**
 * Copyright: 2019
 * Organization: IT-Happens.de
 * @author Ryppo
 */
public class QStats1Controller extends Y3WorkbenchController {

    private Q1Preferences PREFS = UserContext.getInstance().getEnvironment(Q1Preferences.class);

    @FXML
    Tile countQuestionaires_tle, countRatings_tle, theKuh_tle;

    @FXML
    private TableView<QuestionReport> questionaireReport_tbv;

    @FXML
    private TableColumn<QuestionReport, String> questionText_tvc, questionNumer_tvc, averageRating_tvc,
	    minimumRating_tbc, maximumRating_tbc, countRatings_tbc;

    @FXML
    private TableColumn<QuestionReport, String> count1_tbc, count2_tbc, count3_tbc, count4_tbc, count5_tbc;

    @FXML
    private Button createExcelReport_btn;

    private SurveyResult surveyResult;

    private final static int MAX_RATING_VALUE = 4;
    private final static int MATRIX_SCALE = 10;
    private final static int ANIMATION_SPEED = 1000;

    private final DecimalFormat DECIMALFORMAT_RATING = new DecimalFormat("0.0");
    private final DecimalFormat DECIMALFORMAT_COUNT = new DecimalFormat("0");

    @Override
    public void buildUi() {
	reset();
	countQuestionaires_tle.setDecimals(0);
	countQuestionaires_tle.setTickLabelDecimals(0);
	countQuestionaires_tle.setTitle(FxTools.COUNT_FILLED_QUESTIONAIRES);
	countQuestionaires_tle.setAnimationDuration(ANIMATION_SPEED);

	countRatings_tle.setDecimals(1);
	countRatings_tle.setTitle(FxTools.AVERAGE_RATINGS);
	countRatings_tle.setAnimated(true);
	countRatings_tle.setSkinType(SkinType.MATRIX);
	countRatings_tle.setMaxValue(MAX_RATING_VALUE);
	countRatings_tle.setAnimationDuration(ANIMATION_SPEED);

	theKuh_tle.setImage(FxTools.Q1_LOGO);
	theKuh_tle.setSkinType(SkinType.IMAGE);
	theKuh_tle.setImageMask(ImageMask.ROUND);

	questionNumer_tvc.setCellValueFactory(cellData -> cellData.getValue() != null
		? new SimpleStringProperty(Integer.toString(cellData.getValue().idProperty().getValue()))
		: null);
	questionText_tvc.setCellValueFactory(cellData -> cellData.getValue() != null
		? new SimpleStringProperty(cellData.getValue().textProperty().getValue())
		: null);
	averageRating_tvc.setCellValueFactory(cellData -> cellData.getValue() != null
		? new SimpleStringProperty(DECIMALFORMAT_RATING.format(cellData.getValue().ratingProperty().get()))
		: null);
	minimumRating_tbc
		.setCellValueFactory(
			cellData -> cellData.getValue() != null
				? new SimpleStringProperty(
					DECIMALFORMAT_RATING.format(cellData.getValue().lowestRatingProperty().get()))
				: null);
	maximumRating_tbc
		.setCellValueFactory(
			cellData -> cellData.getValue() != null
				? new SimpleStringProperty(
					DECIMALFORMAT_RATING.format(cellData.getValue().highestRatingProperty().get()))
				: null);
	countRatings_tbc.setCellValueFactory(cellData -> cellData.getValue() != null
		? new SimpleStringProperty(DECIMALFORMAT_COUNT.format(cellData.getValue().countRatingsProperty().get()))
		: null);
	count1_tbc.setCellValueFactory(cellData -> cellData.getValue() != null
		? new SimpleStringProperty(DECIMALFORMAT_COUNT.format(cellData.getValue().count1Property().get()))
		: null);
	count2_tbc.setCellValueFactory(cellData -> cellData.getValue() != null
		? new SimpleStringProperty(DECIMALFORMAT_COUNT.format(cellData.getValue().count2Property().get()))
		: null);
	count3_tbc.setCellValueFactory(cellData -> cellData.getValue() != null
		? new SimpleStringProperty(DECIMALFORMAT_COUNT.format(cellData.getValue().count3Property().get()))
		: null);
	count4_tbc.setCellValueFactory(cellData -> cellData.getValue() != null
		? new SimpleStringProperty(DECIMALFORMAT_COUNT.format(cellData.getValue().count4Property().get()))
		: null);
	count5_tbc.setCellValueFactory(cellData -> cellData.getValue() != null
		? new SimpleStringProperty(DECIMALFORMAT_COUNT.format(cellData.getValue().count5Property().get()))
		: null);

	createExcelReport_btn.disableProperty().bind(Bindings.size(questionaireReport_tbv.getItems()).isEqualTo(0));

	reset();

    }

    @Override
    public void openModel(Object model) {
	// TODO Auto-generated method stub

    }

    @Override
    public boolean isCompatibleModel(Object model) {
	// TODO Auto-generated method stub
	return false;
    }

    @Override
    public void reset() {
	countQuestionaires_tle.setAnimated(false);
	countQuestionaires_tle.valueProperty().set(0d);
	countQuestionaires_tle.setAnimated(true);
	countRatings_tle.setAnimated(false);
	countRatings_tle.clearChartData();
	countRatings_tle.setAnimated(true);
	surveyResult = null;
	questionaireReport_tbv.getItems().clear();
    }

    @FXML
    public void actionRefresh() {
	refresh();
    }

    @FXML
    public void actionCreateExcelReport() {
	Task<Void> task = new Task<Void>() {
	    @SuppressWarnings("finally")
	    @Override
	    public Void call() {
		try {
		    activateOverlay(true);
		    File excelFile = TableViewExcelExporter.export(questionaireReport_tbv, FxTools.APP_TITLE,
			    PREFS.getSurveyTitle());
		    if (excelFile != null) {
			Runtime.getRuntime().exec("explorer.exe /select," + excelFile.getAbsolutePath());
		    }
		} catch (Exception ex) {
		    log().error(ex);
		} finally {
		    activateOverlay(false);
		    return null;
		}
	    }

	    private void activateOverlay(boolean activate) {
		updateMessage(Boolean.toString(activate));
	    }
	};
	task.messageProperty().addListener((observale, oldValue, newValue) -> {

	    boolean activateOverlay = Boolean.parseBoolean(newValue);
	    if (activateOverlay) {
		gui().getWorkbench().showOverlay(new Region(), true);
	    } else {
		gui().getWorkbench().clearOverlays();
	    }
	});
	new Thread(task).start();
    }

    @Override
    public void refresh() {
	try {
	    reset();
	    surveyResult = QuestionaireFileControl.loadSurveyResult(PREFS.getSurveyFolder());
	    countQuestionaires_tle.setMaxValue(PREFS.getExpectedCountOfFilledQuestionaires());
	    countQuestionaires_tle.setValue(surveyResult.size());

	    questionaireReport_tbv.getItems().addAll(surveyResult.reportQuestionsResult().questionsProperty());

	    List<ChartData> chartData = new ArrayList<>();
	    int countQuestions = surveyResult.countQuestions();
	    for (int pos = 0; pos < countQuestions; pos++) {
		double averageValue = surveyResult.calculateAverageRating(pos);
		chartData.add(new ChartData(surveyResult.getQuestion(pos).textProperty().get(), averageValue));
	    }
	    countRatings_tle.setMatrixSize(countQuestions, MAX_RATING_VALUE * MATRIX_SCALE);
	    countRatings_tle.setChartData(chartData);
	} catch (Exception e) {
	    e.printStackTrace();
	}
    }

}
