package de.ithappens.q1.module;

import de.ithappens.q1.core.Y3AppController;
import de.ithappens.q1.core.Y3WorkbenchModule;
import de.ithappens.q1.fx.FxTools;
import de.jensd.fx.glyphs.materialdesignicons.MaterialDesignIcon;

public class Q1PreferencesModule extends Y3WorkbenchModule {
	
	public Q1PreferencesModule(Y3AppController mainController) {
		super(mainController, FxTools.Q_PREFERENCES, MaterialDesignIcon.SETTINGS, "q1-preferences.fxml");
	}

}
