package de.ithappens.q1.module;

import de.ithappens.q1.core.Y3AppController;
import de.ithappens.q1.core.Y3WorkbenchModule;
import de.ithappens.q1.fx.FxTools;
import de.jensd.fx.glyphs.materialdesignicons.MaterialDesignIcon;

public class QStats1Module extends Y3WorkbenchModule {
	
	public QStats1Module(Y3AppController mainController) {
		super(mainController, FxTools.Q_STATS, MaterialDesignIcon.CHART_BAR, "q1_stats.fxml");
	}

	@Override
	public void activate_hook() {
		getController().refresh();
	}

}
