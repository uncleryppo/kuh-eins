package de.ithappens.q1.module;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;

import de.ithappens.commons.usercontext.UserContext;
import de.ithappens.q1.Q1Preferences;
import de.ithappens.q1.core.Y3WorkbenchController;
import de.ithappens.q1.fx.FxTools;
import de.ithappens.q1.module.sub.AlphabetSequence;
import de.ithappens.q1.module.sub.PersonalCodePrintableController;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.print.PageLayout;
import javafx.print.PageOrientation;
import javafx.print.Paper;
import javafx.print.Printer;
import javafx.print.PrinterJob;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.transform.Scale;

/**
 * Copyright: 2019
 * Organization: IT-Happens.de
 * @author Ryppo
 */
public class QCodesController extends Y3WorkbenchController {

    private Q1Preferences PREFS = UserContext.getInstance().getEnvironment(Q1Preferences.class);

    @FXML
    private Button print_btn;

    @FXML
    private Label countCodesToPrint_txf, printStatus_txf;

    @FXML
    private VBox preview_vbx;

    @FXML
    private TextField prefix_txf, amountOfCodesToPrint_txf;

    @Override
    public void buildUi() {
	prefix_txf.setOnKeyTyped((e) -> {
	    String value = prefix_txf.getText();
	    if (value.length() > 5) {
		prefix_txf.setText(StringUtils.left(value, 5));
		prefix_txf.positionCaret(5);
	    }
	});
    }

    @FXML
    private void actionPrint() {
	String title = PREFS.getSurveyTitle();
	final String borderStyle = "-fx-border-color: blue;\n" + "-fx-border-width: 3;\n"
		+ "-fx-border-style: dashed;\n";
	preview_vbx.getChildren().clear();
	String prefix = prefix_txf.getText();
	int countOfCodesToPrint = getAmountOfCodesToPrint();
	AlphabetSequence alphabetSequence = new AlphabetSequence();
	VBox printPage_vbx = null;
	int countRowsOfPrintPage = 0;
	HBox row_hbx = null;
	int maxCountRowsPerPage = PREFS.getMaxCodeRowsPerPage();
	List<Node> printPages = new ArrayList<>();
	for (int i = 0; i < countOfCodesToPrint; i++) {
	    if (printPage_vbx == null) {
		printPage_vbx = new VBox();
		countRowsOfPrintPage = 0;
	    }
	    FXMLLoader fxmlLoader = FxTools.getFXMLLoader("personal-code-printable.fxml");
	    Node personalCodePrintableNode = FxTools.loadFxml(fxmlLoader);
	    personalCodePrintableNode.setStyle(borderStyle);
	    PersonalCodePrintableController pcpController = fxmlLoader.getController();
	    pcpController.set(title, prefix + alphabetSequence.next());
	    if (row_hbx == null) {
		row_hbx = new HBox(personalCodePrintableNode);
	    } else {
		row_hbx.getChildren().add(personalCodePrintableNode);
		printPage_vbx.getChildren().add(row_hbx);
		row_hbx = null;
		countRowsOfPrintPage++;
		if (countRowsOfPrintPage == maxCountRowsPerPage || i == countOfCodesToPrint - 1) {
		    // preview_vbx.getChildren().add(printPage_vbx);
		    printPages.add(printPage_vbx);
		    printPage_vbx = null;
		}
	    }
	}
	// printSetup(preview_vbx.getChildren());
	printSetup(printPages);
    }

    private void printSetup(List<Node> printablePages) {
	PrinterJob job = PrinterJob.createPrinterJob();
	if (job == null) {
	    return;
	}
	if (job.showPrintDialog(gui().getWorkbench().getScene().getWindow())) {
	    double layoutTranslate = 0d;
	    for (Node printablePage : printablePages) {
		layoutTranslate = print(job, printablePage, layoutTranslate);
	    }
	}
	job.endJob();
    }

    private double print(PrinterJob job, Node printablePage, double layoutTranslate) {
	printStatus_txf.textProperty().bind(job.jobStatusProperty().asString());
	PageLayout pageLayout = job.getPrinter().createPageLayout(Paper.A4, PageOrientation.PORTRAIT,
		Printer.MarginType.DEFAULT);
	// double scale = (pageLayout.getPrintableWidth() - 20) /
	// printablePage.getBoundsInParent().getWidth();
	// Scale scaleTransform = new Scale(scale, scale);
	Scale scaleTransform = new Scale(0.5, 0.5);
	printablePage.getTransforms().add(scaleTransform);
	// printablePage.getTransforms().add(new Translate(0, -layoutTranslate));
	job.printPage(printablePage);
	printablePage.getTransforms().remove(scaleTransform);
	return printablePage.getLayoutY();
    }

    public int getAmountOfCodesToPrint() {
	String aoctp = amountOfCodesToPrint_txf.getText();
	return NumberUtils.isCreatable(aoctp) ? Integer.parseInt(aoctp) : 0;
    }

    @Override
    public void openModel(Object model) {
	// TODO Auto-generated method stub

    }

    @Override
    public boolean isCompatibleModel(Object model) {
	// TODO Auto-generated method stub
	return false;
    }

    @Override
    public void reset() {
    }

    @Override
    public void refresh() {
	// TODO Auto-generated method stub

    }

}
