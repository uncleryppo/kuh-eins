package de.ithappens.q1.module.sub;

import de.ithappens.q1.fx.RatingPane;
import de.ithappens.q1.model.Question;
import javafx.beans.property.IntegerProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.util.converter.NumberStringConverter;

public class Type01Controller {

    @FXML
    private Label qText_lbl, qId_lbl;
    @FXML
    private HBox questionRow_hbx;

    private Question question;

    private RatingPane rating;

    public void setQuestion(Question _q) {
	question = _q;
	qText_lbl.textProperty().bind(question.textProperty());

	qId_lbl.textProperty().bindBidirectional(question.idProperty(), new NumberStringConverter());
	rating = new RatingPane(4, question.getAnswers(), false);
	rating.setMaxHeight(questionRow_hbx.getHeight());
	rating.setPadding(new Insets(5, 5, 5, 5));
	rating.setSpacing(20d);
	rating.ratingProperty().set(0);
	questionRow_hbx.getChildren().add(rating);
	question.ratingProperty().bind(rating.ratingProperty());

	rating.ratingProperty().addListener(new ChangeListener<Number>() {
	    @Override
	    public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
		System.out.println("Rating changed: " + question.ratingProperty().doubleValue() + " ["
			+ question.idProperty().get() + " | " + question.textProperty().get() + "]");
	    };
	});

    }

    public IntegerProperty getRatingProperty() {
	return rating.ratingProperty();
    }

    public void setRatingValue(int ratingValue) {
	rating.setRating(ratingValue);
    }

}
