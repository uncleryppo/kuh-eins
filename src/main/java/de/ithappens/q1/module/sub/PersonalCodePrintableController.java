package de.ithappens.q1.module.sub;

import javafx.fxml.FXML;
import javafx.scene.control.Label;

public class PersonalCodePrintableController {
	
	@FXML
	private Label title_lbl, code_lbl;

	public void set(String title, String code) {
		title_lbl.setText(title);
		code_lbl.setText(code);
	}
	
}
