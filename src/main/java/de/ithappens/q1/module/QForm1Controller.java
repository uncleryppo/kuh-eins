package de.ithappens.q1.module;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import de.ithappens.commons.usercontext.UserContext;
import de.ithappens.q1.Q1Preferences;
import de.ithappens.q1.core.Y3WorkbenchController;
import de.ithappens.q1.fx.FxTools;
import de.ithappens.q1.model.Question;
import de.ithappens.q1.model.Questionaire;
import de.ithappens.q1.model.QuestionaireFileControl;
import de.ithappens.q1.module.sub.Type01Controller;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;

/**
 * Copyright: 2019
 * 
 * @author Christian.Rybotycky
 */
public class QForm1Controller extends Y3WorkbenchController {

	private Q1Preferences PREFS = UserContext.getInstance().getEnvironment(Q1Preferences.class);

	@FXML
	private Button save_btn;

	@FXML
	private VBox questions_vbx;

	@FXML
	private TextField userCode_txf;

	private List<Type01Controller> questionsController = new ArrayList<>();

	private Questionaire questionaire = new Questionaire();

	@Override
	public void buildUi() {
//		save_btn.setGraphic(new MaterialDesignIconView(MaterialDesignIcon.CHECK));
		save_btn.setStyle("-fx-background-color: \n" + 
				"        blue;\n" + 
				"    -fx-background-insets: 0,1,2,3;\n" + 
				"    -fx-background-radius: 3,2,2,2;\n" + 
				"    -fx-padding: 6 10 6 10;\n" + 
				"    -fx-text-fill: white;\n" + 
				"    -fx-font-size: 12px;");
		parseQuestions();
		for (Question question : questionaire.questionsProperty()) {
			createQuestionNode(question);
		}
	}

	private void createQuestionNode(Question question) {
		FXMLLoader fxmlLoader = FxTools.getFXMLLoader("q-type-01.fxml");
		questions_vbx.getChildren().add(FxTools.loadFxml(fxmlLoader));
		Type01Controller qController = fxmlLoader.getController();
		qController.setQuestion(question);
		questionsController.add(qController);
	}

	private void parseQuestions() {
		try {
			questionaire = QuestionaireFileControl.loadQuestions(PREFS.getLastUsedTemplate(), false);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void writeAnswers() {
		try {
			QuestionaireFileControl.saveQuestionaire(PREFS.getSurveyFolder(), questionaire, userCode_txf.getText());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void openModel(Object model) {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean isCompatibleModel(Object model) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void reset() {
		userCode_txf.setText("");
		for (Type01Controller questionController : questionsController) {
			questionController.setRatingValue(0);
		}
	}

	public void actionSave() {
		if (StringUtils.isNotEmpty(userCode_txf.getText())) {
			if (questionaire.isAllRated()) {
				gui().getWorkbench().showConfirmationDialog(FxTools.SAVE, FxTools.DID_YOU_FINISH, buttonType -> {
					if ("Ja".equals(buttonType.getText())) {
						writeAnswers();
						gui().getWorkbench().showErrorDialog(FxTools.THANK_YOU, FxTools.THE_QUESTIONAIRE_IS_SAVED,
								buttonType1 -> {
									System.out.println("akzeptiert: " + buttonType1.getText());
									reset();
								});
					}
				});
			} else {
				gui().getWorkbench().showErrorDialog(FxTools.NOT_YET, FxTools.FIRST_ANSWER_ALL_QUESTIONS,
						buttonType -> {
							System.out.println("akzeptiert: " + buttonType.getText());
						});
			}
		} else {
			gui().getWorkbench().showErrorDialog(FxTools.NOT_YET, FxTools.FIRST_ENTER_YOUR_CODE, buttonType -> {
				System.out.println("akzeptiert: " + buttonType.getText());
			});
		}
	}

	@Override
	public void refresh() {
		// TODO Auto-generated method stub

	}

}
