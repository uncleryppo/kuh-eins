package de.ithappens.q1;

import java.io.PrintWriter;
import java.io.StringWriter;

import de.ithappens.commons.usercontext.UserContext;
import de.ithappens.q1.fx.FxTools;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class Q1 extends Application {

    public static void main(String args[]) {
	launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
	try {
	    UserContext.getInstance().addEnvironment(new Q1Preferences());
		FXMLLoader loader = new FXMLLoader(getClass().getResource("workbench.fxml"));
	    Parent root = loader.load();
	    primaryStage.setTitle(FxTools.APP_TITLE);
	    primaryStage.getIcons().add(FxTools.APP_ICON);
	    Scene myScene = new Scene(root);
	    primaryStage.setScene(myScene);
		primaryStage.show();

	    Q1Controller control = loader.getController();
	    control.registerModules();
	    control.open(control.getDefaultScope());

	} catch (Exception e) {
		e.printStackTrace();
		Alert alert = new Alert(AlertType.ERROR);
		alert.setTitle("?");
		alert.setHeaderText(e.getMessage());
		StringWriter sw = new StringWriter();
		e.printStackTrace(new PrintWriter(sw));
		String stackTrace = sw.toString();
		alert.getDialogPane().setContent(new VBox(new Label("Stack Trace:"), new TextArea(stackTrace)));
		alert.showAndWait();
	    System.exit(1);
	}
    }

}
