package de.ithappens.q1;

/**
 * Copyright: 2022
 * Organization: IT-Happens.de
 *
 * @author Ryppo
 */
public class Q1Starter {

    public static void main(String args[]) {
        Q1.main(args);
    }
}
