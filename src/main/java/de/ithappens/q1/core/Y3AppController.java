package de.ithappens.q1.core;

import java.awt.Desktop;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.dlsc.workbenchfx.Workbench;
import com.dlsc.workbenchfx.model.WorkbenchModule;
import com.dlsc.workbenchfx.view.controls.NavigationDrawer;
import com.dlsc.workbenchfx.view.controls.PrettyScrollPane;
import com.dlsc.workbenchfx.view.controls.ToolbarItem;

import de.ithappens.commons.usercontext.UserContext;
import de.ithappens.q1.Q1Preferences;
import de.ithappens.q1.fx.FxTools;
import javafx.beans.Observable;
import javafx.beans.binding.Bindings;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.geometry.Side;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuButton;
import javafx.scene.control.MenuItem;
import javafx.scene.control.Skin;
import javafx.scene.control.SkinBase;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TouchEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;

public abstract class Y3AppController {
	
	private Q1Preferences PREFS = UserContext.getInstance().getEnvironment(Q1Preferences.class);

	public enum SCOPE {
		Q_A, REPORT, CODES, PREFS
	}

	@FXML
	private Workbench workbench;

	private Map<SCOPE, Y3WorkbenchModule> modules = new HashMap<>();

	private Logger log = LogManager.getLogger();

	public Workbench getWorkbench() {
		return workbench;
	}

	@FXML
	private void initialize() {
		VBox about_pne = (VBox) FxTools.loadFxml(FxTools.getFXMLLoader(getAboutFxml()));
		workbench.setNavigationDrawer(new CustomNavigationDrawer());
		MenuItem about_mi = new MenuItem(null, about_pne);
		about_mi.setOnAction((e) -> {
		});
		workbench.getNavigationDrawerItems().add(about_mi);
		ImageView brand_imv = new ImageView(getImage());
		workbench.getToolbarControlsLeft().add(new ToolbarItem(brand_imv));
		Label appTitle_lbl = new Label(getApplicationTitle() + " | " + PREFS.getSurveyTitle());
		appTitle_lbl.textProperty().bind(PREFS.surveyTitleProperty());
		appTitle_lbl.setPrefSize(500d, 50d);
		appTitle_lbl.setMinSize(500d, 50d);
		appTitle_lbl.setMaxSize(500d, 50d);
		workbench.getToolbarControlsLeft().add(new ToolbarItem(appTitle_lbl));
		initUi();
	}

	public abstract void initUi();

	public void initModules() {
		registerModules();
	}

	public void register(SCOPE scope, Y3WorkbenchModule module) {
		modules.put(scope, module);
		workbench.getModules().add(module);
		module.init(workbench);
		workbench.openModule(module);
	}
	
	public void unregister(SCOPE scope, Y3WorkbenchModule module) {
		module.destroy();
		if (getWorkbench().getOpenModules().contains(module)) {
			module.close();
		}
		modules.remove(scope);
		workbench.getModules().remove(module);
	}

	public abstract void registerModules();

	public abstract SCOPE getDefaultScope();

	public abstract String getAboutFxml();

	public abstract Image getImage();

	public abstract String getApplicationTitle();

	public void open(SCOPE scope) {
		open(scope, null);
	}

	public void closeAllButDefault() {
		if (!modules.isEmpty()) {
			for (SCOPE scope : modules.keySet()) {
				if (!scope.equals(getDefaultScope())) {
					WorkbenchModule moduleToClose = modules.get(scope);
					ObservableList<WorkbenchModule> openModules = workbench.getOpenModules();
					if (openModules != null && openModules.contains(moduleToClose)) {
						workbench.closeModule(moduleToClose);
					}
				}
			}

		}
	}

	public void open(SCOPE scope, Object model) {
		if (isAllowedToOpen(scope)) {
			Y3WorkbenchModule module = modules.get(scope);
			module.getController().openModel(model);
			workbench.openModule(module);
		} else {
			getWorkbench().showErrorDialog("OPEN MODULE", "FORBIDDEN", buttonType1 -> {
			});
		}
	}

	public abstract boolean isAllowedToOpen(SCOPE scope);

	public void openUrl(String url) {
		if (StringUtils.isNotEmpty(url)) {
			try {
				Desktop.getDesktop().browse(new URI(url));
			} catch (IOException | URISyntaxException e) {
				log.error(e);
			}
		}
	}

	class CustomNavigationDrawer extends NavigationDrawer {

		/**
		 * Creates a navigation drawer control.
		 */
		public CustomNavigationDrawer() {
			super();
		}

		@Override
		protected Skin<?> createDefaultSkin() {
			return new CustomNavigationDrawerSkin(this);
		}

	}

	class CustomNavigationDrawerSkin extends SkinBase<CustomNavigationDrawer> {

		private VBox menuContainer;
		private CustomNavigationDrawer navigationDrawer;
		private VBox drawerBox;
		private BorderPane header;
		private PrettyScrollPane scrollPane;
		private StackPane backIconShape;
		private Button backBtn;
		private Label companyLogo;

		/**
		 * Creates the skin for the {@link CustomNavigationDrawer} control.
		 * 
		 * @param navigationDrawer to create this skin for
		 */
		public CustomNavigationDrawerSkin(CustomNavigationDrawer navigationDrawer) {
			super(navigationDrawer);
			this.navigationDrawer = navigationDrawer;

			initializeParts();
			layoutParts();
			setupEventHandlers();
			setupValueChangedListeners();

			buildMenu();
		}

		/**
		 * Initializes all parts of the skin.
		 */
		private void initializeParts() {
			drawerBox = new VBox();
			drawerBox.getStyleClass().add("drawer-box");

			header = new BorderPane();
			header.getStyleClass().add("header");

			menuContainer = new VBox();
			menuContainer.getStyleClass().add("menu-container");

			scrollPane = new PrettyScrollPane(menuContainer);

			backIconShape = new StackPane();
			backIconShape.getStyleClass().add("shape");
			backBtn = new Button("", backIconShape);
			backBtn.getStyleClass().add("icon");
			backBtn.setId("back-button");

			companyLogo = new Label();
			companyLogo.getStyleClass().add("logo");
		}

		/**
		 * Defines the layout of all parts in the skin.
		 */
		private void layoutParts() {
			drawerBox.getChildren().addAll(header, scrollPane);

			menuContainer.setFillWidth(true);

			header.setTop(backBtn);
			header.setCenter(companyLogo);

			getChildren().add(drawerBox);
		}

		private void setupEventHandlers() {
			backBtn.setOnAction(evt -> navigationDrawer.hide());
		}

		private void setupValueChangedListeners() {
			navigationDrawer.getItems().addListener((Observable it) -> buildMenu());
		}

		private void buildMenu() {
			menuContainer.getChildren().clear();
			for (MenuItem item : getSkinnable().getItems()) {
				if (item instanceof Menu) {
					// item is a submenu
					menuContainer.getChildren().add(buildSubmenu(item));
				} else {
					// item is a regular menu item
					menuContainer.getChildren().add(buildMenuItem(item));
				}
			}
		}

		private MenuButton hoveredBtn;
		private boolean isTouchUsed = false;

		private MenuButton buildSubmenu(MenuItem item) {
			Menu menu = (Menu) item;
			MenuButton menuButton = new MenuButton();
			menuButton.setPopupSide(Side.RIGHT);
			menuButton.graphicProperty().bind(menu.graphicProperty());
			menuButton.textProperty().bind(menu.textProperty());
			menuButton.disableProperty().bind(menu.disableProperty());
			menuButton.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
			menuButton.getStyleClass().addAll(item.getStyleClass());
			Bindings.bindContent(menuButton.getItems(), menu.getItems());

			// To determine if a TOUCH_RELEASED event happens.
			// The MOUSE_ENTERED results in an unexpected behaviour on touch events.
			// Event filter triggers before the handler.
			menuButton.addEventFilter(TouchEvent.TOUCH_RELEASED, e -> isTouchUsed = true);

			// Only when ALWAYS or SOMETIMES
			if (!Priority.NEVER.equals(getSkinnable().getMenuHoverBehavior())) {
				menuButton.addEventHandler(MouseEvent.MOUSE_ENTERED, e -> { // Triggers on hovering over Menu
					if (isTouchUsed) {
						isTouchUsed = false;
						return;
					}
					// When ALWAYS, then trigger immediately. Else check if clicked before (case:
					// SOMETIMES)
					if (Priority.ALWAYS.equals(getSkinnable().getMenuHoverBehavior())
							|| (hoveredBtn != null && hoveredBtn.isShowing())) {
						menuButton.show(); // Shows the context-menu
						if (hoveredBtn != null && hoveredBtn != menuButton) {
							hoveredBtn.hide(); // Hides the previously hovered Button if not null and not self
						}
					}
					hoveredBtn = menuButton; // Add the button as previously hovered
				});
			}
			return menuButton;
		}

		private Button buildMenuItem(MenuItem item) {
			Button button = new Button();
			button.textProperty().bind(item.textProperty());
			button.graphicProperty().bind(item.graphicProperty());
			button.disableProperty().bind(item.disableProperty());
			button.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
			button.getStyleClass().addAll(item.getStyleClass());
			button.setOnAction(item.getOnAction());

			// Only in cases ALWAYS and SOMETIMES: hide previously hovered button
			if (!Priority.NEVER.equals(getSkinnable().getMenuHoverBehavior())) {
				button.addEventHandler(MouseEvent.MOUSE_ENTERED, e -> { // Triggers on hovering over Button
					if (!isTouchUsed) {
						if (hoveredBtn != null) {
							hoveredBtn.hide(); // Hides the previously hovered Button if not null
						}
						hoveredBtn = null; // Sets it to null
					}
				});
			}
			return button;
		}
	}

}
