package de.ithappens.q1.core;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javafx.scene.layout.Region;

public abstract class Y3WorkbenchController {

    private Y3WorkbenchModule gui;
    private Y3AppController mainController;
    private Logger log = LogManager.getLogger();

    public void build(Y3AppController _mainController, Y3WorkbenchModule _gui) {
	gui = _gui;
	mainController = _mainController;
	buildUi();
    }

    public abstract void buildUi();

    public Y3WorkbenchModule gui() {
	return gui;
    }

    public abstract void openModel(Object model);

    public abstract boolean isCompatibleModel(Object model);

    public Logger log() {
	return log;
    }

    public Y3AppController mainController() {
	return mainController;
    }

    public abstract void reset();

    public abstract void refresh();

    public void activateOverlay(boolean activate) {
	if (activate) {
	    gui().getWorkbench().showOverlay(new Region(), true);
	} else {
	    gui().getWorkbench().clearOverlays();
	}
    }

}
