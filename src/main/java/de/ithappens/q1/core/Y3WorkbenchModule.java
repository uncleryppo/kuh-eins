package de.ithappens.q1.core;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.dlsc.workbenchfx.Workbench;
import com.dlsc.workbenchfx.model.WorkbenchModule;

import de.ithappens.q1.fx.FxTools;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon;
import de.jensd.fx.glyphs.materialdesignicons.MaterialDesignIcon;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;

public abstract class Y3WorkbenchModule extends WorkbenchModule {

	private final Y3AppController mainController;
	private Node ui = null;
	private FXMLLoader loader;
	private String fxmlFilename;
	private Y3WorkbenchController control;

	private boolean onceInitDone = false;

	Logger log = LogManager.getLogger();

	public Y3WorkbenchModule(Y3AppController mainController, String name, MaterialDesignIcon icon,
			String fxmlFilename) {
		super(name, icon);
		this.mainController = mainController;
		this.fxmlFilename = fxmlFilename;
	}

	public Y3WorkbenchModule(Y3AppController mainController, String name, FontAwesomeIcon icon, String fxmlFilename) {
		super(name, icon);
		this.mainController = mainController;
		this.fxmlFilename = fxmlFilename;
	}

	public void reset() {
		getController().reset();
	}

	public void initOnce_hook() {
	}

	public void init_hook() {
	}

	@Override
	public void init(Workbench workbench) {
		super.init(workbench);
		if (!onceInitDone) {
			loader = FxTools.getFXMLLoader(fxmlFilename);
			ui = FxTools.loadFxml(loader);
			initController();
			initOnce_hook();
			onceInitDone = true;
		}
		init_hook();
	}

	public Logger log() {
		return log;
	}

	public Y3AppController getMainController() {
		return mainController;
	}

	public Y3WorkbenchController getController() {
		return control;
	}

	/**
	 * Executed BEFORE ExtWorkbenchController.buildUi()
	 */
	public void buildUi_preHook() {
	}

	public void initController() {
		control = getLoader().getController();
		buildUi_preHook();
		control.build(getMainController(), this);
	}

	public FXMLLoader getLoader() {
		return loader;
	}

	public void activate_hook() {
	}

	@Override
	public Node activate() {
		activate_hook();
		return ui;
	}

}
